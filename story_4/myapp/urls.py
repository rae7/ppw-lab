from django.conf.urls import url
from .views import home, contact
from django.conf import settings

urlpatterns = [
    url('index', home),
    url('contact', contact ,name='contact'),
    url('', home)
]

